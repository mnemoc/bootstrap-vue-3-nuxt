import { defineNuxtConfig } from "nuxt";
import eslintPlugin from "vite-plugin-eslint";
import viteStylelint from "@amatlash/vite-plugin-stylelint";

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  srcDir: "src",
  target: "static",

  typescript: {
    strict: true,
  },
  modules: ["bootstrap-vue-3/nuxt"],

  vite: {
    plugins: [
      eslintPlugin({
        fix: true,
      }),
      viteStylelint({}),
    ],
  },
});
